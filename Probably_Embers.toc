## Interface: 60200
## Title: Probably |cffffffff[|cffFF5800Mirakuru's Ember|cffffffff]
## Notes: Custom Probably Profile
## Dependencies: Probably
## Author: Mirakuru
## Version: 0.9-Alpha

-- Main
Embers.lua

-- Library files
Lib\Overloads.lua
Lib\LibBossIDs.lua
Lib\LibStub\LibStub.lua

-- Settings files
GUI\Shadow.lua

-- Priority files
Class\Priest\Shadow.lua