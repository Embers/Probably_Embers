--[[
    Embers - A custom ProbablyEngine Combat Priority
    -- Created by Mirakuru

    Released under the GNU General Public License v3.0
]]
local overloadsFirstRun = false

-- Cache GetSpellInfo
local spellcache = setmetatable({}, {__index=function(t,v) local a = {GetSpellInfo(v)} if GetSpellInfo(v) then t[v] = a end return a end})
local function GetSpellInfo(a)
    return unpack(spellcache[a])
end

function Embers.overloads()
    if not overloadsFirstRun then
        if FireHack then
            CastGround = nil
            LineOfSight = nil
            
            function CastGround(spell, target)
                if UnitExists(target) then
                    local _,_,_,_,_, maxRange,_ = GetSpellInfo(spell)
                    local reachPlayer = UnitCombatReach("player")
                    local reachTarget = UnitCombatReach(target)
                    local combatDistance = Distance(target, "player")
                    local trueDistance = combatDistance + reachTarget + reachPlayer
                    if combatDistance < maxRange and trueDistance > maxRange then
                        local x, y, z = GetPositionBetweenObjects("player", target, maxRange-3)
                        CastSpellByName(spell)
                        ClickPosition(x, y, z, true)
                        CancelPendingSpell()
                        return
                    else
                        local x, y, z = ObjectPosition(target)
                        CastSpellByName(spell)
                        ClickPosition(x, y, z, true)
                        CancelPendingSpell()
                        return
                    end
                end
                if not ProbablyEngine.timeout.check('groundCast') then
                    ProbablyEngine.timeout.set('groundCast', 0.05, function()
                            Cast(spell)
                            if IsAoEPending() then
                                SetCVar("deselectOnClick", "0")
                                CameraOrSelectOrMoveStart(1)
                                CameraOrSelectOrMoveStop(1)
                                SetCVar("deselectOnClick", "1")
                                SetCVar("deselectOnClick", stickyValue)
                                CancelPendingSpell()
                            end
                        end)
                end
            end
            
            function LineOfSight(a, b)
                if UnitExists(a) and UnitExists(b) then
                    local ax, ay, az = ObjectPosition(a)
                    local bx, by, bz = ObjectPosition(b)
                    local losFlags =  bit.bor(0x10, 0x100)
                    local aCheck = select(6,strsplit("-",UnitGUID(a)))
                    local bCheck = select(6,strsplit("-",UnitGUID(b)))
                    local ignoreLOS = {
                        [76585] = true,     -- Ragewing the Untamed (UBRS)
                        [77063] = true,     -- Ragewing the Untamed (UBRS)
                        [77182] = true,     -- Oregorger (BRF)
                        [77891] = true,     -- Grasping Earth (BRF)
                        [77893] = true,     -- Grasping Earth (BRF)
                        [78981] = true,     -- Iron Gunnery Sergeant (BRF)
                        [81318] = true,     -- Iron Gunnery Sergeant (BRF)
                        [83745] = true,     -- Ragewing Whelp (UBRS)
                        [86252] = true,     -- Ragewing the Untamed (UBRS)
                    }
                    
					if ignoreLOS[tonumber(aCheck)] ~= nil then return true end
					if ignoreLOS[tonumber(bCheck)] ~= nil then return true end
                    if ax == nil or ay == nil or az == nil or bx == nil or by == nil or bz == nil then return false end
                    if TraceLine(ax, ay, az+2.25, bx, by, bz+2.25, losFlags) then return false end
                    return true
                end
            end
            
            overloadsFirstRun = true
            print("|cffffffff[|cffFF5800Mirakuru's Ember|cffffffff]|r ProbablyEngine Overloads loaded successfully.")
        end
    end
end