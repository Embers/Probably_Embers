--[[
    Embers - A custom ProbablyEngine Combat Priority
    -- Created by Mirakuru

    Released under the GNU General Public License v3.0
]]
-- Library supervisor
_G["Embers"] = _G["Embers"] or {}
BINDING_HEADER_EMBERS = "Probably |cffffffff[|cffFF5800Mirakuru's Ember|cffffffff]"
ProbablyEngine.library.register('Embers', Embers)


-- Local environment variables
local vt = vt or 0
local swp = swp or 0
local SA_TOTAL = SA_TOTAL or 0

local SA = SA or {}
local objectCache = objectCache or {}

local t17 = t17 or 0
local t18 = t18 or 0
local classTrinket = classTrinket or false
local priestTier17 = {115560,115561,115562,115563,115564}
local priestTier18 = {124155,124161,124166,124172,124178}
local warlockTier17 = {115585,115586,115587,115588,115589}
local warlockTier18 = {124156,124162,124167,124173,124179}

local intBuffs = {}
local critBuffs = {}
local hasteBuffs = {}
local versaBuffs = {}
local multiBuffs = {}
local masteryBuffs = {}

local intBuffCount = intBuffCount or 0
local critBuffCount = critBuffCount or 0
local hasteBuffCount = hasteBuffCount or 0
local versaBuffCount = versaBuffCount or 0
local multiBuffCount = multiBuffCount or 0
local masteryBuffCount = masteryBuffCount or 0


-- Cache GetSpellInfo
local spellcache = setmetatable({}, {__index=function(t,v) local a = {GetSpellInfo(v)} if GetSpellInfo(v) then t[v] = a end return a end})
local function GetSpellInfo(a)
    return unpack(spellcache[a])
end


-- Rounding a number to N decimals
function Embers.round(num, idp)
	return tonumber(string.format("%." .. (idp or 0) .. "f", num))
end


-- Debugging function. Takes a table argument, if no argument given - shows object cache
function inspectTable(option)
    local DiesalGUI = LibStub("DiesalGUI-1.0")
    local explore = DiesalGUI:Create('TableExplorer')
    local option = nil
    
    if not option then explore:SetTable("Table Checker", objectCache)
        else explore:SetTable("Table Checker", option) end
    
    explore:BuildTree()
end


-- Buiild settings frame
Embers.settings = function(frame)
	if not shown then
		ref = ProbablyEngine.interface.buildGUI(frame)
		shown = true
		state = true
		ref.parent:SetEventListener('OnClose', function() shown = false state = false end)
	elseif shown and state then ref.parent:Hide() state = false
	elseif shown and not state then ref.parent:Show() state = true end
end


-- Filter for object scanner
local function filterObjects(object)
    local inLOS = LineOfSight("player",object)
    local canAttack = UnitCanAttack("player",object)
    local currentTarget = UnitIsUnit("target",object)
    local alive = ProbablyEngine.condition["alive"](object)
    local distance = ProbablyEngine.condition["distance"](object)
    local inCombat = UnitAffectingCombat(object)
    local isTapped = (UnitIsTappedByPlayer(object) or UnitIsTappedByAllThreatList(object))
    local mapID = GetCurrentMapAreaID
    
    -- Proving Grounds ...
    if mapID() == 899 then isTapped = true inCombat = true end
    
    if ObjectExists(object) and not currentTarget and canAttack and alive and distance < 40 and inLOS and inCombat and isTapped then return true end
    return false
end


-- Scan for available objects
local function scanObjects(callback)
    if FireHack and IterateObjects then
        wipe(objectCache)
        IterateObjects(function(object)
            if (not callback or callback and callback(object)) and filterObjects(object) then
                objectCache[#objectCache+1] = object
            end
        end, ObjectTypes.Unit)
    end
end


-- Get an object from cache
local function getCachedObject(callback)
    if not callback or #objectCache < 1 then return false end
    for i=1,#objectCache do
        if callback(objectCache[i]) then return objectCache[i] end
    end
    return false
end


-- Frame that updates object cache every (timer.int) seconds
local timer = CreateFrame("Frame")
timer.int = 0.5
timer:SetScript("OnUpdate",function(self, elapsed)
    local isEnabled = ProbablyEngine.interface.fetchKey('shadowCFG','enableObjects')
    if not FireHack then return end
    if ProbablyEngine.config.read('button_states', 'MasterToggle', false) then
        self.ttl = (self.ttl or 0) + elapsed
        if (self.ttl > self.int) then
            if not not isEnabled then scanObjects() end
            self.ttl = 0
        end
    else if #objectCache > 0 then wipe(objectCache) end return end
end)


-- ProbablyEngine was not coded for objects, this solves that and allocates work
Embers.objectSupervisor = function(spell, unitType, ignore)
    local filter = false
    local GetHaste = GetHaste
    local round = Embers.round
    local UnitDebuff = UnitDebuff
    local speed = GetUnitSpeed("player")
    local isCasting = UnitCastingInfo("player")
    
    -- User not using FireHack
    if not FireHack then return false end
    
    -- Shadow Word: Pain
    if spell == 589 then
        filter = function(obj)
            if swp <= 6 and not UnitDebuff(obj,GetSpellInfo(spell),nil,"PLAYER") then return true end
            return false
        end
    end
    
    -- Vampiric Touch
    if spell == 34914 then
        filter = function(obj)
            if #objectCache >= 5 or speed > 0 or vt >= 5 or isCasting then return false end
            if not UnitDebuff(obj,GetSpellInfo(spell),nil,"PLAYER") then return true end
            return false
        end
    end
    
    -- Devouring Plague
    if spell == 2944 then
        filter = function(obj)
            if ignore then return true end
            if not UnitDebuff(obj,GetSpellInfo(158831),nil,"PLAYER") then return true end
            return false
        end
    end
    
    -- Shadow Word: Death
    if spell == 32379 then
        filter = function(obj)
            local floor = math.floor
            local UnitHealth = UnitHealth
            local UnitHealthMax = UnitHealthMax
            local orbs = UnitPower("player",SPELL_POWER_SHADOW_ORBS)
            
            if orbs < 5 and floor((UnitHealth(obj) / UnitHealthMax(obj)) * 100) < 20 then return true end
            return false
        end
    end
    
    -- Get an object
    local obj = getCachedObject(filter)
    
    -- Pass the object to PE's targeting engine
    if obj then
        if spell ~= 32379 then
            ProbablyEngine.dsl.parsedTarget = obj
            return true
        else Cast(spell,obj) return true end
    end
    
    return false
end



-- Listen for changed equipment events
ProbablyEngine.listener.register("PLAYER_EQUIPMENT_CHANGED", function(...)
        local _,_,class = UnitClass("player")
        
        if class == 5 then
            if t17 ~= 0 then t17 = 0 end
            if t18 ~= 0 then t18 = 0 end
            classTrinket = IsEquippedItem(124519)
            for i=1,#priestTier17 do if IsEquippedItem(priestTier17[i]) then t17 = t17+1 end end
            for i=1,#priestTier18 do if IsEquippedItem(priestTier18[i]) then t18 = t18+1 end end
        elseif class == 9 then
            if t17 ~= 0 then t17 = 0 end
            if t18 ~= 0 then t18 = 0 end
            classTrinket = IsEquippedItem(124522)
            for i=1,#warlockTier17 do if IsEquippedItem(warlockTier17[i]) then t17 = t17+1 end end
            for i=1,#warlockTier18 do if IsEquippedItem(warlockTier18[i]) then t18 = t18+1 end end
        end
end)


-- Update events after zoning
ProbablyEngine.listener.register("PLAYER_ENTERING_WORLD", function(...)
        local _,_,class = UnitClass("player")
        
        if class == 5 then
            if t17 ~= 0 then t17 = 0 end
            if t18 ~= 0 then t18 = 0 end
            classTrinket = IsEquippedItem(124519)
            for i=1,#priestTier17 do if IsEquippedItem(priestTier17[i]) then t17 = t17+1 end end
            for i=1,#priestTier18 do if IsEquippedItem(priestTier18[i]) then t18 = t18+1 end end
        elseif class == 9 then
            if t17 ~= 0 then t17 = 0 end
            if t18 ~= 0 then t18 = 0 end
            classTrinket = IsEquippedItem(124522)
            for i=1,#warlockTier17 do if IsEquippedItem(warlockTier17[i]) then t17 = t17+1 end end
            for i=1,#warlockTier18 do if IsEquippedItem(warlockTier18[i]) then t18 = t18+1 end end
        end
end)

-- Entering combat status event
ProbablyEngine.listener.register("PLAYER_REGEN_DISABLED", function(...)
        SA_TOTAL = 0
end)
-- Leaving combat status event
ProbablyEngine.listener.register("PLAYER_REGEN_ENABLED", function(...)
        SA_TOTAL = 0
end)


-- Listen to combat events
ProbablyEngine.listener.register("COMBAT_LOG_EVENT_UNFILTERED", function(...)
        local _,event,_,sourceGUID,_,_,_,destGUID,_,_,_,spellID = ...
        local player = UnitGUID("player")
        
        if destGUID == player then
            if event == "SPELL_AURA_APPLIED" then
                if intBuffs[spellID] then intBuffCount = intBuffCount + 1 end
                if hasteBuffs[spellID] then hasteBuffCount = hasteBuffCount + 1 end
                if critBuffs[spellID] then critBuffCount = critBuffCount + 1 end
                if masteryBuffs[spellID] then masteryBuffCount = masteryBuffCount + 1 end
                if versaBuffs[spellID] then versaBuffCount = versaBuffCount + 1 end
                if multiBuffs[spellID] then multiBuffCount = multiBuffCount + 1 end
            end
            if event == "SPELL_AURA_REMOVED" then
                if intBuffs[spellID] then intBuffCount = intBuffCount - 1 end
                if hasteBuffs[spellID] then hasteBuffCount = hasteBuffCount - 1 end
                if critBuffs[spellID] then critBuffCount = critBuffCount - 1 end
                if masteryBuffs[spellID] then masteryBuffCount = masteryBuffCount - 1 end
                if versaBuffs[spellID] then versaBuffCount = versaBuffCount - 1 end
                if multiBuffs[spellID] then multiBuffCount = multiBuffCount - 1 end
            end
        end
        
        if event == "UNIT_DIED" or event == "UNIT_DESTROYED" or event == "SPELL_INSTAKILL" then
            if SA[destGUID] then
                SA_TOTAL = SA_TOTAL - SA[destGUID].count
                if SA_TOTAL < 0 then SA_TOTAL = 0 end
                SA[destGUID].count = nil
                SA[destGUID] = nil
            end
        end
        
        if event == "SPELL_CAST_SUCCESS" then
            if spellID == 147193 then
                if not SA[destGUID] or SA[destGUID] == nil then
                    SA[destGUID] = {}
                    SA[destGUID].count = 0
                end
                SA_TOTAL = SA_TOTAL + 1
                SA[destGUID].count = SA[destGUID].count + 1
            end
            if spellID == 155521 then
                SA_TOTAL = SA_TOTAL - 1
                if SA[destGUID] and SA[destGUID].count > 0 then
                    SA[destGUID].count = SA[destGUID].count - 1
                    if SA[destGUID].count <= 0 then
                        SA[destGUID].count = nil
                        SA[destGUID] = nil
                    end
                end
            end
        end
        
        
        if event == "SPELL_AURA_APPLIED" then
            if spellID == 589 then swp = swp + 1 end
            if spellID == 34914 then vt = vt + 1 end
        end
        
        if event == "SPELL_AURA_REMOVED" then
            if spellID == 589 then swp = swp - 1 end
            if spellID == 34914 then vt = vt - 1 end
        end
end)



-- Custom condition: Return number of active enemies from cache
ProbablyEngine.condition.register("enemies", function()
        return #objectCache
end)

-- Custom condition: Return boolean for equipped class trinket
ProbablyEngine.condition.register("class.trinket", function()
        return classTrinket
end)

-- Custom condition: Return number of equipped Tier 17 set items
ProbablyEngine.condition.register("tier17", function()
        return t17
end)

-- Custom condition: Return number of equipped Tier 18 set items
ProbablyEngine.condition.register("tier18", function()
        return t18
end)

-- Custom condition: Return number of Shadowy Apparitions en route to targets
ProbablyEngine.condition.register("sa", function()
        return SA_TOTAL
end)

-- Custom condition: Return boolean if target is boss
ProbablyEngine.condition.register("isBoss", function (target, spell)
        local boss = LibStub("LibBossIDs")
        local classification = UnitClassification(target)
        if spell == "true" and (classification == "rareelite" or classification == "rare") then return true end
        if classification == "worldboss" or UnitLevel(target) == -1 or boss.BossIDs[UnitID(target)] then return true end
        return false
end)

-- Custom condition: Return number of active Intellect effects
ProbablyEngine.condition.register("int.buffs", function()
        return intBuffCount
end)

-- Custom condition: Return number of active Critical Strike effects
ProbablyEngine.condition.register("crit.buffs", function()
        return critBuffCount
end)

-- Custom condition: Return number of active Haste effects
ProbablyEngine.condition.register("haste.buffs", function()
        return hasteBuffCount
end)

-- Custom condition: Return number of active Versatility effects
ProbablyEngine.condition.register("versatility.buffs", function()
        return versaBuffCount
end)

-- Custom condition: Return number of active Multistrike effects
ProbablyEngine.condition.register("multistrike.buffs", function()
        return multiBuffCount
end)

-- Custom condition: Return number of active Mastery effects
ProbablyEngine.condition.register("mastery.buffs", function()
        return masteryBuffCount
end)

-- Custom condition: Return number of active Buff effects
ProbablyEngine.condition.register("any.buff", function()
        return intBuffCount+critBuffCount+hasteBuffCount+versaBuffCount+multiBuffCount+masteryBuffCount
end)